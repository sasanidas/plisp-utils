(de _source ("Ed" . "Prg")
   (ifn "X"
      (eval
         (out (pil "editor")
            (println (cons 'load "Ed")) ) )
      (when (pair "X")
         (setq C (cdr "X")  "X" (car "X")) )
      (when
         (setq "*Ed"
            (if C
               (get C '*Dbg -1 "X")
               (get "X" '*Dbg 1) ) )
         (out (tmp "tags")
            (let D (pack (pwd) "/")
               (for Lst
                  (group  # (file (line . sym) (line . sym) ..)
                     (extract
                        '((This)
                           (when (: *Dbg)
                              (cons (path (cdar @)) (caar @) This) ) )
                        (all) ) )
                  (let Tags
                     (in (car Lst)
                        (let (Line 1  Ofs 0)
                           (mapcar
                              '((X)
                                 (do (- (car X) Line)
                                    (inc 'Ofs (inc (size (line T)))) )
                                 (pack
                                    `(pack "^J" (char 127))
                                    (cdr X)
                                    (char 1)
                                    (setq Line (car X))
                                    ","
                                    Ofs ) )
                              (sort (cdr Lst)) ) ) )
                     (prinl
                        "^L^J"
                        (unless (= `(char "/") (char (car Lst))) D)
                        (car Lst)
                        ","
                        (sum size Tags)
                        Tags ) ) ) ) )
         (run "Prg") ) )
   "X" )

(de get-path ("X" C)
   (_source
      '("@lib/eled.l" "@lib/eedit.l")
	    (println (pack (car "*Ed") ":" (path (cdr "*Ed"))))
             ) ) 
