
(when (featurep 'inferior-plisp) 
    (require 'inferior-plisp))


(defgroup plisp-utils nil
  "PicoLisp utilities for programming"
  :prefix "plisp-utils-"
  :link '(url-link :tag "Repository" "https://gitlab.com/sasanidas/plisp-utils"))


(defcustom plisp-utils-pil-exec "/usr/bin/pil"
  "PicoLisp pil executable location."
  :type 'file
  :group 'plisp-utils)

(defvar plisp-utils-find-path-file (concat
				     (file-name-directory load-file-name)
				     "find-path.l")
  "Default location for the plisp utils PicoLisp find file.")


(defun plisp-utils-eval-buffer ()
  "Eval the current buffer."
  (interactive)
  (if (featurep 'inferior-plisp)
      (inferior-plisp-load-file (buffer-file-name))
   (message "inferior-plisp is not available")))


(defun plisp-utils--load-libraries ()
  "Search for load function inside the buffer.
Append it to a temp file, and return the file name."
  (let* ((plisp-temp-l (make-temp-file "plisp_l.l"))
	 (load-lines
	  (-filter (lambda (line)
				(posix-string-match "\(load\s+[\"][@]?[[:word:]]+[\\/]?[[:word:]]+\\.l[\"]" line))
			      (s-lines (buffer-substring-no-properties  1 (buffer-end 1))))))
    (-map (lambda (line)
	     (write-region line nil plisp-temp-l 'append 0)
	    )
	  load-lines)
    (format "%s" plisp-temp-l)))

(when (and  (featurep 'inferior-plisp)
	    (featurep 'major-mode-hydra)
	   (featurep 'plisp-mode))

(major-mode-hydra-define plisp-mode nil
  ("Eval"
   (("b" plisp-utils-eval-buffer  "buffer")
    ("r" inferior-plisp-send-region "region")
    ("f" inferior-plisp-send-definition "function"))
   "REPL"
   (("i" inferior-plisp-run-picolisp "run-picolisp"))
   "Doc"
   (("d" plisp-describe-symbol "describe-symbol")
    ("g" plisp-utils-find-definition "go to source")))))


(defun plisp-utils-find-definition ()
  "Open file the file where the symbol is define."
  (interactive)
  (let* ((plisp-symbol (thing-at-point 'symbol))
	 (library-file (plisp-utils--load-libraries))
	 (symbol-location
	  (split-string
	   (string-trim-right (string-trim
			       (shell-command-to-string
				(concat plisp-utils-pil-exec " "
					library-file " "
					 plisp-utils-find-path-file " "
					"-\"get-path '"plisp-symbol "\" -bye + 2> /dev/null"))
					   "\"" "\n")
			      "\"") ":")))
    (if (> (length symbol-location) 0 )
	(with-current-buffer  (pop-to-buffer (find-file-noselect (nth 1 symbol-location)))
	  (goto-char (point-min))
        (forward-line (string-to-number (nth 0 symbol-location))))
      (message "Can't find symbol %s location" plisp-symbol))))

(provide 'plisp-utils)
